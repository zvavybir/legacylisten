use std::path::Path;

use anyhow::{Context, Error};
use diskit::{diskit_extend::DiskitExt, open_options::OpenOptions, Diskit};

// Look in lib.rs for justification.
#[allow(clippy::needless_pass_by_value)]
pub fn ensure_file_existence<D>(home: &Path, diskit: D) -> Result<(), Error>
where
    D: Diskit,
{
    let base = home.join(".zvavybir/legacylisten");
    diskit
        .create_dir_all(&base)
        .with_context(|| format!("Couldn't create config dir: {base:?}"))?;
    diskit
        .open_with_options(
            base.join("conffile.csv"),
            OpenOptions::new().write(true).create(true),
        )
        .context("Couldn't open or create config file")?;
    diskit
        .open_with_options(
            base.join("songs.csv"),
            OpenOptions::new().write(true).create(true),
        )
        .context("Couldn't open or create song data file")?;

    Ok(())
}
