use std::io::{ErrorKind, Read};

pub struct UnicodeReader<R>
where
    R: Read,
{
    inner: R,
}

impl<R> UnicodeReader<R>
where
    R: Read,
{
    pub const fn new(inner: R) -> Self
    {
        Self { inner }
    }
}

impl<R> Iterator for UnicodeReader<R>
where
    R: Read,
{
    type Item = Result<char, Vec<u8>>;

    fn next(&mut self) -> Option<Self::Item>
    {
        let mut read = [0; 4];

        match self.inner.read_exact(&mut read[0..1])
        {
            Err(err) if err.kind() == ErrorKind::UnexpectedEof => return None,
            Err(_) => return Some(Err(vec![])),
            Ok(()) =>
            {}
        }

        // The number of leading ones determine what kind of byte it is:
        // * 0: ASCII
        // * 1: Middle of UTF8 byte sequence
        // * n >= 2: Start of UTF8 byte sequence with n bytes
        let leading_ones = read[0].leading_ones() as _;

        if leading_ones == 0
        {
            Some(Ok(read[0] as char))
        }
        else if (2..=4).contains(&leading_ones)
        {
            if self.inner.read_exact(&mut read[1..leading_ones]).is_err()
            {
                return Some(Err(vec![read[0]]));
            }

            if read[1..leading_ones].iter().any(|x| x >> 6 != 2)
            {
                return Some(Err(read[0..leading_ones].to_owned()));
            }

            let mut sum = (read[0] & ((1 << (7 - leading_ones)) - 1)) as _;

            for val in &read[1..leading_ones]
            {
                sum <<= 6;
                sum += (val & ((1 << 6) - 1)) as u32;
            }

            char::from_u32(sum).map_or_else(
                || Some(Err(read[0..leading_ones].to_owned())),
                |c| Some(Ok(c)),
            )
        }
        else
        {
            Some(Err(vec![read[0]]))
        }
    }
}

#[cfg(test)]
mod test
{

    use rand::{rngs::StdRng, Rng, SeedableRng};

    use super::UnicodeReader;

    #[test]
    fn unicode_reading_test()
    {
        // Setup
        let mut buf = vec![0u8; 1024 * 64];
        StdRng::seed_from_u64(3_141_592_653_589_793_238).fill(&mut buf[..]);

        // Test
        for chunk_len in 1..10
        {
            for chunk in buf.chunks(chunk_len)
            {
                if std::str::from_utf8(chunk).is_ok()
                {
                    assert_eq!(
                        UnicodeReader::new(chunk).collect::<Result<String, _>>(),
                        Ok(String::from_utf8(chunk.to_owned()).unwrap())
                    );
                }
            }
        }
    }
}
