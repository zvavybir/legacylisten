use std::{
    cmp::min,
    io::{self, ErrorKind, Read, Seek, SeekFrom},
};

use anyhow::{Context, Error};

pub struct Buffer
{
    buf: Vec<u8>,
    pos: usize,
}

impl Read for Buffer
{
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, io::Error>
    {
        if self.pos >= self.buf.len()
        {
            return Ok(0);
        }
        let slice = &self.buf[self.pos..];
        let slice = &slice[..min(buf.len(), slice.len())];
        let len = min(slice.len(), buf.len());

        (buf[..len]).copy_from_slice(&slice[..len]);

        self.pos += len;

        Ok(len)
    }
}

impl Seek for Buffer
{
    fn seek(&mut self, pos: io::SeekFrom) -> Result<u64, io::Error>
    {
        match pos
        {
            SeekFrom::Current(x) =>
            {
                self.pos = self
                    .pos
                    .checked_add_signed(x.try_into().map_err(|_| ErrorKind::InvalidInput)?)
                    .ok_or(ErrorKind::InvalidInput)?;
            }
            SeekFrom::Start(x) =>
            {
                self.pos = x.try_into().map_err(|_| ErrorKind::InvalidInput)?;
            }
            SeekFrom::End(x) =>
            {
                if x >= 0
                {
                    return Err(ErrorKind::InvalidInput.into());
                }

                self.pos = self
                    .buf
                    .len()
                    .checked_sub(-x as _)
                    .ok_or(ErrorKind::InvalidInput)?;
            }
        }

        Ok(self.pos as _)
    }
}

impl Buffer
{
    pub fn new<R: Read>(mut reader: R) -> Result<Self, Error>
    {
        let mut buf = Self {
            buf: vec![],
            pos: 0,
        };
        reader
            .read_to_end(&mut buf.buf)
            .context("Couldn't read from given reader")?;

        Ok(buf)
    }
}

#[cfg(test)]
mod test
{

    use diskit::{diskit_extend::DiskitExt, VirtualDiskit};
    use std::{
        error::Error,
        io::{Read, Seek, SeekFrom, Write},
    };

    use super::Buffer;

    #[test]
    fn buffer_read_test() -> Result<(), Box<dyn Error>>
    {
        // Setup
        let diskit = VirtualDiskit::default();
        diskit.create("/test")?.write_all(b"This is a test!")?;

        // Read
        let mut buffer = Buffer::new(diskit.open("/test")?)?;
        let mut buf = [0; 7];

        assert_eq!(buffer.read(&mut buf)?, 7);
        assert_eq!(&buf, b"This is");
        assert_eq!(buffer.read(&mut buf[..2])?, 2);
        assert_eq!(&buf, b" ais is");

        buffer.seek(SeekFrom::Current(-3))?;
        assert_eq!(buffer.read(&mut buf[..2])?, 2);
        assert_eq!(&buf, b"s is is");

        buffer.seek(SeekFrom::Start(6))?;
        assert_eq!(buffer.read(&mut buf[..5])?, 5);
        assert_eq!(&buf, b"s a tis");

        buffer.seek(SeekFrom::End(-3))?;
        assert_eq!(buffer.read(&mut buf[..3])?, 3);
        assert_eq!(&buf, b"st! tis");

        Ok(())
    }
}
