//! Enables custom configuration if used as a library.

use std::{
    mem::take,
    path::{Path, PathBuf},
};

use anyhow::{Context, Error};
use diskit::Diskit;

use crate::{csv::Csv, l10n::Lang, LlError};

/// Enables custom configuration of legacylisten if used as a library.
///
/// If legacylisten is used as a library this struct can be used to
/// emulate a configuration file if you don't want to use a hardware
/// config file.
///
/// These things mean exactly what was already described in the
/// README.  The text here is the mostly same as there.
#[derive(Clone, Debug)]
pub struct Conffile
{
    /// If you have your music collection somewhere else (like me on
    /// an external hard drive or in `~/Music`) you can use this
    /// option to change the directory `legacylisten` will search.
    /// The `~/` notation is not usable in the configuration file,
    /// even under *NIX systems.
    pub data_dir: PathBuf,
    /// The threshold for the [low memory
    /// handler](crate#low-memory-handler) in bytes.
    pub minimum_ram: u64,
    /// Disables the low memory handler (possible values are `true`
    /// and `false`).  If this is set (currently the default)
    /// `minimum_ram` is ignored.
    pub ignore_ram: bool,
    /// `legacylisten` supports basic internationalization and this is
    /// the option to activate it.  See [Lang] for possible values.
    pub lang: Lang,
    /// Repeating a song is usually a sign that the song is good and
    /// should be played more often, but it's very easy to forget to
    /// increase it's playing likelihood, so this option does this
    /// automatically.  Everytime a song is repeated it's increased by
    /// the configured value.  You can also set a negative value.
    pub repeat_bonus: i64,
    /// Enables the dbus module.  D-Bus/MPRIS is responsible for
    /// integrating `legacylisten` into your system nicely.  This is
    /// not necessary to get `legacylisten` working.  The default
    /// value is `false`.
    pub enable_dbus: bool,
    /// Specifies which characters are used for the commands.  Any
    /// characters (i.e. unicode code points) can be used except '?',
    /// the newline character and the classical decimal digits.
    pub command_characters: Vec<char>,
}

impl Conffile
{
    /// Returns the default configuration.
    ///
    /// This function returns the default configuration and is
    /// therefor the config used when nothing is said otherwise.
    ///
    /// This is a good basis of your own configuration.
    #[must_use]
    pub fn default(conffile_dir: &Path) -> Self
    {
        Self {
            data_dir: conffile_dir.join("data"),
            minimum_ram: 1024 * 1024 * 1024,
            ignore_ram: true,
            lang: Lang::English,
            repeat_bonus: 0,
            enable_dbus: false,
            command_characters: (0..26).map(|x| (x + b'a') as char).collect(),
        }
    }

    /// Get configured configuration.
    ///
    /// This function reads the configuration the user wrote in the
    /// config file.
    ///
    /// Use this if you want to (partly) respect your users wishes.
    ///
    /// # Errors
    /// Returns an error if the file couldn't be read.
    // False positive of pedantic lint.  The unwraps in the
    // "command_characters" part can't fail as it is checked.
    #[allow(clippy::missing_panics_doc)]
    pub fn get_user_config<D>(conffile_dir: &Path, diskit: D) -> Result<Self, Error>
    where
        D: Diskit,
    {
        let mut rv = Self::default(conffile_dir);

        let mut csv = Csv::new(conffile_dir.join("conffile.csv"), diskit)
            .context("Couldn't open conffile")?;

        for line in &mut csv.entries
        {
            match line[0].as_str()
            {
                "data_dir" => rv.data_dir = PathBuf::from(take(&mut line[1])),
                "minimum_ram" =>
                {
                    let _ = line[1].parse().map(|x| rv.minimum_ram = x);
                }
                "ignore_ram" =>
                {
                    let _ = line[1].parse().map(|x| rv.ignore_ram = x);
                }
                "lang" => match line[1].to_lowercase().as_str()
                {
                    "english" => rv.lang = Lang::English,
                    "german" | "deutsch" => rv.lang = Lang::German,
                    "esperanto" => rv.lang = Lang::Esperanto,
                    "dutch" | "nederlands" => rv.lang = Lang::Dutch,
                    "custom" =>
                    {
                        let path = line[2].parse();
                        let id = line[3].parse();

                        if let (Ok(path), Ok(id)) = (path, id)
                        {
                            rv.lang = Lang::UserCustom(path, id);
                        }
                    }
                    _ =>
                    {}
                },
                "repeat_bonus" =>
                {
                    let _ = line[1].parse().map(|x| rv.repeat_bonus = x);
                }
                "enable_dbus" =>
                {
                    let _ = line[1].parse().map(|x| rv.enable_dbus = x);
                }
                "command_characters" =>
                {
                    fn is_illegal_char(c: char, current: &[char]) -> bool
                    {
                        current.iter().any(|&old| c == old)
                            || c.is_ascii_digit()
                            || c == '\n'
                            || c == '?'
                    }

                    rv.command_characters = vec![];

                    for field in &line[1..]
                    {
                        if field.len() == 1
                        {
                            let c = field.chars().next().unwrap();
                            if !is_illegal_char(c, &rv.command_characters)
                            {
                                rv.command_characters.push(c);
                            }
                        }
                        else if field.len() == 3 && field.chars().nth(1).unwrap() == '-'
                        {
                            let start = field.chars().next().unwrap();
                            let end = field.chars().nth(2).unwrap();

                            for c in start..=end
                            {
                                if !is_illegal_char(c, &rv.command_characters)
                                {
                                    rv.command_characters.push(c);
                                }
                            }
                        }
                        else
                        {
                            return Err(LlError::MalformattedListOfCommandCharacters)
                                .context("Given list of command characters are impossible");
                        }
                    }
                }
                _ =>
                {}
            }
        }

        Ok(rv)
    }

    /// Gets standard configuration.
    ///
    /// Returns the [user's](Self::get_user_config) configuration, or
    /// – if not possible – the [default](Conffile::default)
    /// configuration.
    #[must_use]
    pub fn new<D>(conffile_dir: &Path, diskit: D) -> Self
    where
        D: Diskit,
    {
        Self::get_user_config(conffile_dir, diskit).unwrap_or_else(|_| Self::default(conffile_dir))
    }
}
