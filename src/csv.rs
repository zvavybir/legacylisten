use std::{
    fmt::{self, Display, Formatter},
    mem,
    path::Path,
    sync::Arc,
};

use anyhow::{Context, Error};
use diskit::{diskit_extend::DiskitExt, Diskit};

use crate::{
    config::ArcConfig,
    l10n::L10n,
    songs::{L10nHelper, Song, Songs},
    LlError,
};

#[derive(Clone, Debug)]
pub struct Csv
{
    pub entries: Vec<Vec<String>>,
}

#[derive(Debug, Clone, Copy)]
enum CsvParserState
{
    StartOfEntry,
    Normal,
    EntryQuoted,
    InEscaping,
}

impl Display for Csv
{
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error>
    {
        for line in &self.entries
        {
            let mut first = true;
            for field in line
            {
                if !first
                {
                    write!(f, ",")?;
                }
                first = false;

                // This quotes always everything.  This isn't
                // necessary, but makes life easier.
                write!(f, "\"")?;
                for c in field.chars()
                {
                    match c
                    {
                        '\"' => write!(f, "\"\"")?,
                        _ => write!(f, "{c}")?,
                    }
                }
                write!(f, "\"")?;
            }
            writeln!(f)?;
        }

        Ok(())
    }
}

impl Csv
{
    // Out of backwards compatibility reasons this is still here.
    fn read_pseudo_csv(buf: &[u8]) -> Result<Self, Error>
    {
        let mut csv = vec![];
        let mut entry = vec![];
        let mut field = vec![];
        let mut quoted = false;

        for &c in buf
        {
            if quoted
            {
                field.push(c);
                quoted = false;
            }
            else if c == b'\\'
            {
                quoted = true;
                continue;
            }
            else if c == b','
            {
                entry.push(
                    String::from_utf8(mem::take(&mut field))
                        .context("Couldn't convert csv field to UTF8")?,
                );
            }
            else if c == b'\n'
            {
                if !field.is_empty()
                {
                    return Err(LlError::MalformattedSongsCsv)
                        .context("Lines must be terminated by commas in pseudo-CSV");
                }
                csv.push(mem::take(&mut entry));
            }
            else
            {
                field.push(c);
            }
        }

        Ok(Self { entries: csv })
    }

    // Look in lib.rs for justification.
    #[allow(clippy::needless_pass_by_value)]
    pub(crate) fn new<P: AsRef<Path>, D>(path: P, diskit: D) -> Result<Self, Error>
    where
        D: Diskit,
    {
        // All variants are used and this is so per definitionem, so I
        // think that's better.  Also it's a pedantic lint.
        #[allow(clippy::enum_glob_use)]
        use CsvParserState::*;

        let buf = diskit
            .read_to_string(path)
            .context("Couldn't read CSV file to string")?;

        let mut csv = vec![];
        let mut line = vec![];
        let mut field = String::new();

        let mut state = StartOfEntry;

        for c in buf.chars()
        {
            match (state, c)
            {
                (StartOfEntry, '"') => state = EntryQuoted,
                // Attempt to read as pseudo-CSV.
                (Normal, '"') =>
                {
                    return Self::read_pseudo_csv(buf.as_bytes()).context(
                        "Attempted to read CSV file as pseudo-CSV \
			 due to unexpected quote, but it also failed",
                    )
                }
                (StartOfEntry | Normal | InEscaping, ',') =>
                {
                    line.push(mem::take(&mut field));
                    state = StartOfEntry;
                }
                (StartOfEntry | Normal | InEscaping, '\n') =>
                {
                    if field.is_empty() && line.is_empty()
                    {
                        // Empty lines are skiped.
                        continue;
                    }
                    line.push(mem::take(&mut field));
                    csv.push(mem::take(&mut line));
                    state = StartOfEntry;
                }
                (StartOfEntry | Normal, _) =>
                {
                    field.push(c);
                    state = Normal;
                }
                (EntryQuoted, '"') => state = InEscaping,
                (EntryQuoted, _) | (InEscaping, '"') =>
                {
                    field.push(c);
                    state = EntryQuoted;
                }
                // Attempt to read as pseudo-CSV.
                (InEscaping, _) =>
                {
                    return Self::read_pseudo_csv(buf.as_bytes()).context(
                        "Attempted to read CSV file as pseudo-CSV \
			 due to non-ending quote, but it also failed",
                    )
                }
            }
        }

        if !field.is_empty()
        {
            line.push(mem::take(&mut field));
        }
        if !line.is_empty()
        {
            csv.push(mem::take(&mut line));
        }

        Ok(Self { entries: csv })
    }

    #[must_use]
    pub(crate) fn from<D>(songs: &Songs<D>) -> Self
    where
        D: Diskit,
    {
        let x = songs
            .songs
            .iter()
            .map(|song| {
                vec![
                    song.name.clone(),
                    song.num.to_string(),
                    song.loud.to_string(),
                ]
            })
            .collect();

        Self { entries: x }
    }

    pub(crate) fn get_songs<D>(
        self,
        config: Arc<ArcConfig>,
        l10n: L10n,
        diskit: D,
    ) -> Result<Songs<D>, Error>
    where
        D: Diskit,
    {
        if self.entries.iter().all(|song| song.len() == 3)
        {
            Ok(Songs {
                songs: self
                    .entries
                    .into_iter()
                    .map(|mut song| Song {
                        name: mem::take(&mut song[0]),
                        num: song[1].parse().unwrap(),
                        loud: song[2].parse().unwrap(),
                    })
                    .collect(),
                config,
                l10n_helper: L10nHelper::new(l10n),
                diskit,
            })
        }
        else
        {
            Err(LlError::MalformattedSongsCsv).context("Not exactly three fields in songs file")
        }
    }
}

#[cfg(test)]
mod test
{
    use std::{error::Error, io::Write};

    use diskit::{diskit_extend::DiskitExt, VirtualDiskit};

    use super::Csv;

    #[test]
    fn csv_test() -> Result<(), Box<dyn Error>>
    {
        // Setup
        let diskit = VirtualDiskit::default();
        diskit
            .create("/test1")?
            .write_all("abc,def,ghi\n123,456,789\näöü,+*~".as_bytes())?;
        diskit
            .create("/test2")?
            .write_all("abc,def,ghi\n123,\"456\",789\näöü,+*~\n".as_bytes())?;
        diskit.create("/test3")?.write_all(b"123,456,789")?;
        diskit.create("/test4")?.write_all(b"123,456,789\n")?;
        diskit.create("/test5")?.write_all(b"")?;
        diskit.create("/test6")?.write_all(b"\n")?;
        diskit.create("/test7")?.write_all(b" \n ")?;
        diskit
            .create("/test8")?
            .write_all(b"\"a\",\"\",,\"\\\\\",\\")?;
        diskit
            .create("/test9")?
            .write_all(b"\"a\",\"\",,\"\\\\\",\\\n")?;
        diskit.create("/test10")?.write_all(b"\"\"\"\"")?;

        // Read

        assert_eq!(
            Csv::new("/test1", diskit.clone())?.entries,
            vec![
                vec!["abc", "def", "ghi"],
                vec!["123", "456", "789"],
                vec!["äöü", "+*~"],
            ]
        );

        assert_eq!(
            Csv::new("/test2", diskit.clone())?.entries,
            vec![
                vec!["abc", "def", "ghi"],
                vec!["123", "456", "789"],
                vec!["äöü", "+*~"],
            ]
        );

        assert_eq!(
            Csv::new("/test3", diskit.clone())?.entries,
            vec![vec!["123", "456", "789"],]
        );

        assert_eq!(
            Csv::new("/test4", diskit.clone())?.entries,
            vec![vec!["123", "456", "789"],]
        );

        assert_eq!(
            Csv::new("/test5", diskit.clone())?.entries,
            Vec::<Vec<String>>::new()
        );

        assert_eq!(
            Csv::new("/test6", diskit.clone())?.entries,
            Vec::<Vec<String>>::new()
        );

        assert_eq!(
            Csv::new("/test7", diskit.clone())?.entries,
            vec![vec![" "], vec![" "]]
        );

        assert_eq!(
            Csv::new("/test8", diskit.clone())?.entries,
            vec![vec!["a", "", "", "\\\\", "\\"]]
        );

        assert_eq!(
            Csv::new("/test9", diskit.clone())?.entries,
            vec![vec!["a", "", "", "\\\\", "\\"]]
        );

        assert_eq!(
            Csv::new("/test10", diskit.clone())?.entries,
            vec![vec!["\""]]
        );

        // Format

        assert_eq!(
            Csv::new("/test1", diskit.clone())?.to_string(),
            "\"abc\",\"def\",\"ghi\"\n\"123\",\"456\",\"789\"\n\"äöü\",\"+*~\"\n"
        );

        assert_eq!(
            Csv::new("/test2", diskit.clone())?.to_string(),
            "\"abc\",\"def\",\"ghi\"\n\"123\",\"456\",\"789\"\n\"äöü\",\"+*~\"\n"
        );

        assert_eq!(
            Csv::new("/test3", diskit.clone())?.to_string(),
            "\"123\",\"456\",\"789\"\n"
        );

        assert_eq!(
            Csv::new("/test4", diskit.clone())?.to_string(),
            "\"123\",\"456\",\"789\"\n"
        );

        assert_eq!(Csv::new("/test5", diskit.clone())?.to_string(), "");

        assert_eq!(Csv::new("/test6", diskit.clone())?.to_string(), "");

        assert_eq!(
            Csv::new("/test7", diskit.clone())?.to_string(),
            "\" \"\n\" \"\n"
        );

        assert_eq!(
            Csv::new("/test8", diskit.clone())?.to_string(),
            "\"a\",\"\",\"\",\"\\\\\",\"\\\"\n"
        );

        assert_eq!(
            Csv::new("/test9", diskit.clone())?.to_string(),
            "\"a\",\"\",\"\",\"\\\\\",\"\\\"\n"
        );

        assert_eq!(Csv::new("/test10", diskit)?.to_string(), "\"\"\"\"\n");

        Ok(())
    }
}
