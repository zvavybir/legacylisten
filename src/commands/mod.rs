//! Module for handling commands.

use std::{convert::TryFrom, fmt, sync::Arc};

use legacytranslate::MessageHandler;
use num_enum::TryFromPrimitive;

use crate::{
    config::ArcConfig,
    l10n::{
        messages::{LogLevel, Message},
        L10n,
    },
};

pub(crate) mod impls;

/// Commands
///
/// These are the available commands.
#[repr(usize)]
#[derive(Clone, Copy, PartialEq, Eq, TryFromPrimitive)]
pub enum Command
{
    IncreaseLikelihood,
    DecreaseLikelihood,
    Quit,
    Pause,
    Resume,
    Skip,
    IncreaseVolume,
    DecreaseVolume,
    ShowDuration,
    SwitchPlayPause,
    QuitAfterSong,
    PauseAfterSong,
    ShowInfo,
    OpenCover,
    DisableRepeat,
    RepeatOnce,
    RepeatForever,
    SkipToPrevious,
    Stereo,
    Mono,
    MonoMixed,
}

#[derive(Clone, Copy)]
struct DisplayCommand(Command, L10n);

impl fmt::Display for DisplayCommand
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error>
    {
        write!(f, "{}", self.1.get(Message::Description(self.0)))
    }
}

impl Command
{
    pub(crate) fn show_help(lang: L10n, config: &Arc<ArcConfig>)
    {
        lang.write(Message::HelpHeader);
        for (i, command) in (0..).map_while(|i| Self::try_from(i).map(|c| (i, c)).ok())
        {
            config.l10n.write(Message::LiteralString(
                format!(
                    "{}): {}",
                    config
                        .conffile
                        .command_characters
                        .get(i)
                        .map_or('?', |c| *c),
                    DisplayCommand(command, lang)
                ),
                LogLevel::Println,
            ));
        }
    }
}
