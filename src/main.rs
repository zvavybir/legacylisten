// I think using default() is actually easier to understand.
#![allow(clippy::default_constructed_unit_structs)]

use std::io;

use anyhow::{Context, Error};
use crossbeam_channel::bounded;
use diskit::StdDiskit;
use legacylisten::{
    api::ApiRequester, audio::RodioAudioHandler, conffile::Conffile, l10n::standard_write_handler,
    runner,
};
use simple_logger::SimpleLogger;

fn main() -> Result<(), Error>
{
    SimpleLogger::new()
        .init()
        .expect("Couldn't initialize logger");

    runner::run::<_, RodioAudioHandler, _, _>(
        Conffile::new,
        standard_write_handler(),
        || io::stdin().lock(),
        ApiRequester::new().1,
        bounded(1).1,
        StdDiskit::default(),
    )
    .context("Error with running legacylisten")
}
