//! Handles internationalization and some localisation of
//! legacylisten.

use std::{io::Read, iter::once, path::PathBuf};

use anyhow::{Context, Error};
use diskit::{diskit_extend::DiskitExt, Diskit};
use legacytranslate::Lang as LangTrait;
use unic_langid::LanguageIdentifier;

pub use legacytranslate::{standard_write_handler, L10n, Writer};

pub mod messages;

pub static ENGLISH_L10N_STR: &str = include_str!("english.flt");
pub static GERMAN_L10N_STR: &str = include_str!("de.flt");
pub static ESPERANTO_L10N_STR: &str = include_str!("eo.flt");
pub static DUTCH_L10N_STR: &str = include_str!("nl.flt");
pub static ENGLISH_L10N_LANG_ID: &str = "en-US";
pub static GERMAN_L10N_LANG_ID: &str = "de-DE";
pub static ESPERANTO_L10N_LANG_ID: &str = "eo-EO";
pub static DUTCH_L10N_LANG_ID: &str = "nl-NL";

/// Selection of available languages.
///
/// This struct defines which language is used.  You can use a normal
/// language or give a path or string and a language identifier.
///
/// If not specified otherwise the backup language is always English.
#[derive(Clone, Debug)]
pub enum Lang
{
    English,
    German,
    Esperanto,
    Dutch,
    UserCustom(PathBuf, LanguageIdentifier),
    /// The second element is a reference to the language that should
    /// be used as a backup in case the main one doesn't have a
    /// specific (or – in the worst-case – any) message.
    ProgrammerCustom(String, Option<Box<Lang>>, LanguageIdentifier),
}

impl LangTrait for Lang
{
    fn deconstruct_lang_id<D>(self, diskit: D) -> Result<Vec<(String, LanguageIdentifier)>, Error>
    where
        D: Diskit,
    {
        Ok(match self
        {
            Self::English => vec![(
                String::from(ENGLISH_L10N_STR),
                ENGLISH_L10N_LANG_ID
                    .parse()
                    .context("Coudln't parse English translation")?,
            )],
            Self::German => vec![
                (
                    String::from(GERMAN_L10N_STR),
                    GERMAN_L10N_LANG_ID
                        .parse()
                        .context("Couldn't parse German translation")?,
                ),
                (
                    String::from(ENGLISH_L10N_STR),
                    ENGLISH_L10N_LANG_ID
                        .parse()
                        .context("Couldn't parse English replace translation")?,
                ),
            ],
            Self::Esperanto => vec![
                (
                    String::from(ESPERANTO_L10N_STR),
                    ESPERANTO_L10N_LANG_ID
                        .parse()
                        .context("Couldn't parse Esperanto translation")?,
                ),
                (
                    String::from(ENGLISH_L10N_STR),
                    ENGLISH_L10N_LANG_ID
                        .parse()
                        .context("Couldn't parse English replace translation")?,
                ),
            ],
            Self::Dutch => vec![
                (
                    String::from(DUTCH_L10N_STR),
                    DUTCH_L10N_LANG_ID
                        .parse()
                        .context("Couldn't parse Esperanto translation")?,
                ),
                (
                    String::from(ENGLISH_L10N_STR),
                    ENGLISH_L10N_LANG_ID
                        .parse()
                        .context("Couldn't parse English replace translation")?,
                ),
            ],
            Self::UserCustom(path, lang) =>
            {
                let mut buf = String::new();
                diskit
                    .open(&path)
                    .with_context(|| format!("Couldn't open user translation: {path:?}"))?
                    .read_to_string(&mut buf)
                    .context("Couldn't read user translation")?;

                vec![
                    (buf, lang),
                    (
                        String::from(ENGLISH_L10N_STR),
                        ENGLISH_L10N_LANG_ID
                            .parse()
                            .context("Couldn't parse English replace translation")?,
                    ),
                ]
            }
            Self::ProgrammerCustom(s, backup, lang) => once((s, lang))
                .chain(
                    backup
                        .into_iter()
                        .flat_map(|x| x.deconstruct_lang_id(diskit.clone()))
                        .flat_map(IntoIterator::into_iter),
                )
                .collect(),
        })
    }
}
