use std::{error, fmt};

/// `legacylisten`'s Error type.
///
/// This type bundles all errors which could occur.
#[derive(Debug)]
pub enum Error
{
    /// The CSV file which stores the playing likelihoods and
    /// the volumes was malformatted.
    MalformattedSongsCsv,
    MalformattedListOfCommandCharacters,
    Custom(String),
    Vec(Vec<Error>),
}

impl fmt::Display for Error
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error>
    {
        match self
        {
            Self::MalformattedSongsCsv => write!(f, "Malformatted songs.csv file"),
            Self::MalformattedListOfCommandCharacters =>
            {
                write!(f, "Malformatted list of command characters")
            }
            Self::Custom(err) => write!(f, "Custom error: {err}"),
            Self::Vec(v) =>
            {
                if v.len() == 1
                {
                    write!(f, "{}", v[0])
                }
                else
                {
                    writeln!(f, "Multiple errors ({}):", v.len())?;
                    for (i, e) in v.iter().enumerate()
                    {
                        write!(f, "{i}: {e}")?;
                        if i != v.len() + 1
                        {
                            writeln!(f)?;
                        }
                    }

                    Ok(())
                }
            }
        }
    }
}

impl error::Error for Error {}

impl From<String> for Error
{
    fn from(err: String) -> Self
    {
        Self::Custom(err)
    }
}

impl<T> From<Vec<T>> for Error
where
    Self: From<T>,
{
    fn from(err: Vec<T>) -> Self
    {
        Self::Vec(err.into_iter().map(Into::into).collect())
    }
}
