//! Module for handling audio.
//!
//! This module handles audio and let's you manipulate the playing of
//! songs.  You could for example use this module to copy everything
//! that is played or play some things faster.

use std::{
    fmt::Debug,
    io::{BufReader, Read},
    path::Path,
    sync::{atomic::Ordering, Arc},
    thread,
    time::Duration,
};

use anyhow::{Context, Error};
use crossbeam_channel::{bounded, Receiver};
use diskit::{diskit_extend::DiskitExt, Diskit};
use id3::{Tag, TagLike};
use legacytranslate::MessageHandler;
use mp3_metadata::MP3Metadata;
use ogg_metadata::{read_format, AudioMetadata, OggFormat};
use rodio::{Decoder, OutputStream, OutputStreamHandle, Sink, Source};

use crate::{
    buffer::Buffer,
    config::ArcConfig,
    l10n::messages::{LogLevel, Message},
};

/// Playback interface.
///
/// This trait handles the playing of songs.  Types that implement it
/// provide playback capabilities like [`play`](Self::play)-ing and
/// [`pause`](Self::pause)-ing or [`append`](Self::append)-ing songs
/// to the list of playing songs.
// I think it sounds better.
#[allow(clippy::module_name_repetitions)]
pub trait AudioHandler
{
    /// Creates a new instance of this type.
    ///
    /// This creates a new audio handler.  Please notice the peculiar
    /// return type.
    // This is close enough to `Self`.
    #[allow(clippy::new_ret_no_self)]
    fn new() -> Box<dyn AudioHandler>
    where
        Self: Sized;

    /// Pauses playback
    fn pause(&mut self);

    /// Restarts playback
    fn play(&mut self);

    /// Sets the volume
    ///
    /// Sets the volume of the song.  `1.0` means `100 %`.
    fn set_volume(&mut self, value: f32);

    /// Appends a source
    ///
    /// Appends a [`ChannelSource`] to the list of things played.
    fn append(&mut self, source: ChannelSource);

    /// Skips all songs
    ///
    /// Skips all songs i.e. deletes the list of
    /// [`append`](Self::append)-ed songs.
    fn skip(&mut self);

    /// Checks whether list is empty
    ///
    /// Checks whether the list of songs to playback is empty or not.
    fn empty(&mut self) -> bool;
}

/// The standard [`AudioHandler`]
///
/// This is the standard `AudioHandler`, use it if you don't want to
/// change something.
pub struct RodioAudioHandler
{
    stream_handle: OutputStreamHandle,
    _stream: OutputStream,
    sink: Sink,
}

impl AudioHandler for RodioAudioHandler
{
    fn new() -> Box<dyn AudioHandler>
    where
        Self: Sized,
    {
        let (stream, stream_handle) = OutputStream::try_default().unwrap();
        let sink = Sink::try_new(&stream_handle).unwrap();

        Box::new(Self {
            stream_handle,
            _stream: stream,
            sink,
        })
    }

    fn pause(&mut self)
    {
        self.sink.pause();
    }

    fn play(&mut self)
    {
        self.sink.play();
    }

    fn set_volume(&mut self, value: f32)
    {
        self.sink.set_volume(value);
    }

    fn append(&mut self, source: ChannelSource)
    {
        self.sink.append(source);
    }

    fn skip(&mut self)
    {
        self.sink = Sink::try_new(&self.stream_handle).unwrap();
    }

    fn empty(&mut self) -> bool
    {
        self.sink.empty()
    }
}

/// The kind of output
///
/// This struct saves whether the audio output is normal or forcibly
/// mono (and if then whether just one channel is outputted or all
/// channels are averaged).
#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub enum MonoState
{
    Normal,
    Mono,
    MonoMixed,
}

/// The audio source
///
/// This is the rodio [`Source`](`rodio::Source`) used in
/// legacylisten.  It can be interacted with with the trait methods on
/// [`Source`] and [`Iterator`](std::iter::Iterator).
pub struct ChannelSource
{
    channels: u16,
    sample_rate: u32,
    data_rx: Receiver<(usize, i16)>,
    config: Arc<ArcConfig>,
}

// I think it sounds better.
#[allow(clippy::module_name_repetitions)]
pub(crate) struct ChannelAudio
{
    pub sample_rate: u32,
    pub inner: Option<ChannelSource>,
    pub config: Arc<ArcConfig>,
}

// Look in lib.rs for justification.
#[allow(clippy::needless_pass_by_value)]
fn get_size<P, D>(path: P, diskit: D) -> Result<usize, Error>
where
    D: Diskit,
    P: AsRef<Path> + Debug,
{
    let decoder2 = Decoder::new(
        Buffer::new(
            diskit
                .open(path.as_ref())
                .with_context(|| format!("Couldn't open song: {path:?}"))?,
        )
        .context("Couldn't create new buffer")?,
    )
    .context("Couldn't decode song")?;

    Ok(decoder2.count())
}

// Look in lib.rs for justification.
#[allow(clippy::needless_pass_by_value)]
fn get_ogg_len<P: AsRef<Path>, D>(path: P, diskit: D) -> Option<Duration>
where
    D: Diskit,
{
    // `read_format` returns a `Vec` of `OggFormat`s, so after opening
    // and reading we have to iterate over it (and extract the
    // durations out of it).  As far as I understood it the correct
    // value is the sum, so we do this.
    let dur = read_format(BufReader::new(diskit.open(path.as_ref()).ok()?))
        .unwrap_or_default()
        .into_iter()
        .filter_map(|ogg| match ogg
        {
            OggFormat::Vorbis(metadata) => metadata.get_duration(),
            OggFormat::Opus(metadata) => metadata.get_duration(),
            _ => None,
        })
        .sum::<Duration>();

    // The problem with using the `Iterator::sum` method is that it
    // never returns `None` even if no data was available, so we check
    // against the default value to see if it should be `None`.
    if dur == Duration::new(0, 0)
    {
        None
    }
    else
    {
        Some(dur)
    }
}

// Look in lib.rs for justification.
#[allow(clippy::needless_pass_by_value)]
fn read_mp3_metadata_from_file<P: AsRef<Path>, D>(diskit: D, path: P) -> Option<MP3Metadata>
where
    D: Diskit,
{
    let mut buf = vec![];
    diskit.open(path).ok()?.read_to_end(&mut buf).ok()?;

    mp3_metadata::read_from_slice(&buf).ok()
}

impl ChannelAudio
{
    pub fn new<P, D>(path: P, config: Arc<ArcConfig>, diskit: D) -> Result<Self, Error>
    where
        D: Diskit + Send + 'static,
        P: AsRef<Path> + Debug,
    {
        let (data_tx, data_rx) = bounded(64);

        let decoder = Decoder::new(
            Buffer::new(
                diskit
                    .open(path.as_ref())
                    .with_context(|| format!("Couldn't open song: {path:?}"))?,
            )
            .context("Couldn't create new buffer")?,
        )
        .context("Couldn't decode song")?;
        let channels = decoder.channels();
        let sample_rate = decoder.sample_rate();

        config.current_len.store(0, Ordering::SeqCst);
        config.current_pos.store(0, Ordering::SeqCst);
        config
            .sample_rate
            .store(sample_rate as usize, Ordering::SeqCst);
        config.channels.store(channels as usize, Ordering::SeqCst);

        // To get the duration of a song, we can just decode it once
        // over and count the samples.  The advantage is that this
        // works always, the disadvantage that it takes long and is
        // inefficient, so we first try to persuade the song to tell
        // us it's length voluntarily.  This is done by using rodio's
        // `Decoder::size_hint` (works (nearly?) never, but I already
        // implemented it) and `Decoder::total_duration` (working
        // currently only on wavs and flacs) methods, mp3_metadata's
        // `read_from_file` (obviously only working on mp3s) method
        // and ogg_metadata's `read_format` (working for vorbis and
        // opus, although rodio doesn't support decoding opus, so this
        // is technically superfluous) method.
        let mut size_already_safed = false;
        if let (min, Some(max)) = decoder.size_hint()
        {
            if min == max && min != 0
            {
                config.current_len.store(min, Ordering::SeqCst);
                size_already_safed = true;
            }
        }
        else if let Some(dur) = read_mp3_metadata_from_file(diskit.clone(), path.as_ref())
            .map(|mp3_meta| mp3_meta.duration)
            .or_else(|| decoder.total_duration())
            .or_else(|| get_ogg_len(&path, diskit.clone()))
        {
            // `Config::current_len` is in samples not in seconds or
            // `Duration`, so we need to convert it.
            config.current_len.store(
                (dur.as_secs_f64() * sample_rate as f64 * channels as f64) as usize,
                Ordering::SeqCst,
            );
            size_already_safed = true;
        }

        // This decoding creates the samples which at the end actually
        // are played.
        thread::spawn(move || {
            // Decodes all samples and sends them enumerated as long
            // as this is possible.
            decoder
                .enumerate()
                .all(|sample| data_tx.send(sample).is_ok());
        });

        // Creates a new thread, checks if the size is known by now
        // and if not decodes the complete song to get it.  It only is
        // saved if the next song is not already played (as determined
        // by whether the fetch_add was already once executed again).
        let path = path.as_ref().to_path_buf();
        let config2 = config.clone();
        let current_index = config.monotonic_song_index.fetch_add(1, Ordering::SeqCst) + 1;
        thread::spawn(move || {
            if !size_already_safed
            {
                if let Ok(size) = get_size(path, diskit)
                {
                    if config2.monotonic_song_index.load(Ordering::SeqCst) == current_index
                    {
                        config2.current_len.store(size, Ordering::SeqCst);
                        config2.update_dbus.store(true, Ordering::SeqCst);
                    }
                }
            }
        });

        Ok(Self {
            sample_rate,
            config: config.clone(),
            inner: Some(ChannelSource {
                channels,
                sample_rate,
                data_rx,
                config,
            }),
        })
    }

    pub fn get_pos(&self) -> usize
    {
        self.config.current_pos.load(Ordering::SeqCst)
    }

    pub fn samples_len(&self) -> Option<usize>
    {
        let len = self.config.current_len.load(Ordering::SeqCst);

        if len == 0
        {
            None
        }
        else
        {
            Some(len)
        }
    }
}

impl Iterator for ChannelSource
{
    type Item = i16;

    fn next(&mut self) -> Option<Self::Item>
    {
        let monostate = *self.config.monostate.lock().expect("Bug in legacylisten");
        let num_samples = if monostate == MonoState::Normal
        {
            1
        }
        else
        {
            self.channels as i32
        };
        let samples = (0..num_samples)
            .map(|_| self.data_rx.recv())
            .collect::<Result<Vec<_>, _>>()
            .ok()?;

        let rv = if monostate == MonoState::MonoMixed
        {
            (samples.iter().map(|(_, val)| *val as i32).sum::<i32>() / num_samples) as i16
        }
        else
        {
            samples[(num_samples - 1) as usize].1
        };

        self.config
            .current_pos
            .store(samples[(num_samples - 1) as usize].0, Ordering::SeqCst);

        Some(rv)
    }
}

impl Source for ChannelSource
{
    fn current_frame_len(&self) -> Option<usize>
    {
        let mut update_monostate = self
            .config
            .update_monostate
            .lock()
            .expect("Bug in legacylisten");

        if let Ok(new_monostate) = self.config.rx_monostate.try_recv()
        {
            *update_monostate = true;
            let mut monostate = self.config.monostate.lock().expect("Bug in legacylisten");
            *monostate = new_monostate;
        }

        if *update_monostate
        {
            *update_monostate = false;
            Some(1)
        }
        else
        {
            Some(512)
        }
    }

    fn channels(&self) -> u16
    {
        if *self.config.monostate.lock().expect("Bug in legacylisten") == MonoState::Normal
        {
            self.channels
        }
        else
        {
            1
        }
    }

    fn sample_rate(&self) -> u32
    {
        self.sample_rate
    }

    fn total_duration(&self) -> Option<Duration>
    {
        None
    }
}

fn print_tag<H>(tag: &Tag, l10n: &H)
where
    H: MessageHandler<Message>,
{
    macro_rules! simple {
        ( $l10n : tt , $method : tt , $name : tt ) => {
            if let Some(v) = tag.$method()
            {
                //println!("{}: {}", $l10n.get($name, vec![]), v);
                $l10n.write(Message::$name(v.to_string()));
            }
        };
    }

    l10n.write(Message::PrintPlayingSong);

    simple!(l10n, title, Title);
    simple!(l10n, album, Album);
    simple!(l10n, artist, Artist);
    simple!(l10n, album_artist, AlbumArtist);
    simple!(l10n, year, Year);
    simple!(l10n, genre, Genre);
    simple!(l10n, date_recorded, DateRecorded);
    simple!(l10n, date_released, DateReleased);
    simple!(l10n, disc, Disc);
    simple!(l10n, total_discs, DiscsTotal);
    simple!(l10n, track, Track);
    simple!(l10n, total_tracks, TracksTotal);
    simple!(l10n, duration, Duration);

    for v in tag.lyrics()
    {
        l10n.write(Message::Lyrics(v.clone()));
    }
    for v in tag.synchronised_lyrics()
    {
        l10n.write(Message::SyncLyrics(format!("{v:?}")));
    }
    for v in tag.comments()
    {
        l10n.write(Message::Comment(v.clone()));
    }

    l10n.write(Message::NumPictures(tag.pictures().count()));

    if tag.extended_links().next().is_some()
    {
        l10n.write(Message::ExtLinks);
        for v in tag.extended_links()
        {
            l10n.write(Message::LiteralString(v.to_string(), LogLevel::Println));
        }
    }
    if tag.extended_texts().next().is_some()
    {
        l10n.write(Message::ExtTexts);
        for v in tag.extended_texts()
        {
            l10n.write(Message::LiteralString(v.to_string(), LogLevel::Println));
        }
    }
}

pub(crate) fn print_info<H>(tag: Option<&Result<Tag, id3::Error>>, l10n: &H)
where
    H: MessageHandler<Message>,
{
    match tag
    {
        Some(Ok(tag)) => print_tag(tag, l10n),
        Some(Err(err)) =>
        {
            l10n.write(Message::MetadataUnsupported(err.to_string()));
        }
        None => l10n.write(Message::PrintInfoUnreachable),
    }
}
