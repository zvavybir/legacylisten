use crossbeam_channel::TryRecvError;
use diskit::Diskit;
use legacytranslate::MessageHandler;

use crate::{config::Config, l10n::messages::Message};

#[derive(Copy, Clone, Debug)]
pub enum BigAction
{
    Quit,
    Skip,
    Nothing,
}

pub fn main_match<D>(config: &mut Config, diskit: D) -> BigAction
where
    D: Diskit,
{
    match config.rx.try_recv()
    {
        Ok((_, 0)) | Err(TryRecvError::Empty) => BigAction::Nothing,
        Ok((com, count)) => com.get_handler()(count, config, diskit),
        Err(TryRecvError::Disconnected) =>
        {
            config.l10n.write(Message::CommandReadingProblem);
            BigAction::Quit
        }
    }
}
