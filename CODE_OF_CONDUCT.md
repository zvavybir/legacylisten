Motivated by the belief that software can help improve the world, this
project was created.  As such it's developer community sees themself
bound by the principles of human-centered software and human rights,
and vehemently opposes all kinds of oppression and discrimination, be
they sexism, racism, ableism (be it based on mental (e.g. neurotype)
or physical characteristics), queerphobia (e.g. homo-, trans-,
acephobia), ageism, capitalism (including classism,
(settler/neo-)colonialism and imperialism) or any other kind of
kyriarchal oppression not named here.  Behavior contrary to this will
not be tolerated and appropriate mechanisms will be taken to prevent
the project from becoming a Nazi Bar.

Aware that this developer community is not free from error, there were
several ways of contacting a representative of the community
established, in the hope of fixing any issue that did or might come
up:

* Per [project
  issue](https://codeberg.org/zvavybir/legacylisten/issues/new) (*be
  aware that this is completely public*!),
* Per [e-mail](mailto:codeofconduct@zvavybir.eu) to the main developer, or
* Per [the contact page](https://zvavybir.eu/contact) of the main
  developer's private page (apart from any identifying information
  provided in the message, this is completely anonymous).

Issues or ideas for improvement of this Code of Conduct can be raised
in the same manner.
